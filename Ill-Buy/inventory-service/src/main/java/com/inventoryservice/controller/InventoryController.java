package com.inventoryservice.controller;

import com.inventoryservice.dto.InventoryResponse;
import com.inventoryservice.model.Inventory;
import com.inventoryservice.service.InventoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/inventory")
public class InventoryController {

    private final InventoryService inventoryService;

    @Autowired
    public InventoryController(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }

    @GetMapping("/getAll")
    @ResponseStatus(HttpStatus.OK)
    public List<Inventory> getAll() {
        return inventoryService.getAllInventory();
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public List<InventoryResponse> isInStock(@RequestParam List<String> skuCode) {
        log.info("Received request for skuCode:{}", skuCode);
        return inventoryService.isInStock(skuCode);
    }

//    @GetMapping("/getAll")
//    @ResponseStatus(HttpStatus.OK)
//    public List<Inventory> getAllProducts() {return inventoryService.getAllProducts();}
//
//    @GetMapping("/test")
//    @ResponseStatus(HttpStatus.OK)
//    public void addTest(){
//        inventoryService.addTestProducts();
//    }
//    //Crud
//    @GetMapping("/{id}")
//    public Inventory getUser(@PathVariable UUID id) {return inventoryService.getProductById(id); }

//    @PostMapping
//    @ResponseStatus(HttpStatus.CREATED)
//    public void createProduct(@RequestBody InventoryRequest inventoryRequest){
//        inventoryService.createProduct(inventoryRequest);
//    }


}
