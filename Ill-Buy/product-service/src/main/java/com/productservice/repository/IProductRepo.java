package com.productservice.repository;

import com.productservice.model.Product;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

@ComponentScan
@Repository
public interface IProductRepo extends JpaRepository<Product, UUID> {

    Product getProductBySkuCode(String skuCode);
    void deleteProductByUuid(UUID id);

    List<Product> getProductByProductName(String productName);
    List<Product> getAllByCategory(String category);
}
