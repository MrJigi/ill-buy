package com.productservice.controller;

import com.productservice.dto.Product.ProductRequest;
import com.productservice.dto.Product.ProductResponse;
import com.productservice.model.Product;
import com.productservice.service.ProductService;
//import jakarta.servlet.annotation.HttpConstraint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService){this.productService = productService;}

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public List<Product> getAll() {return productService.getAllProducts();}

    @GetMapping("/getAll")
    @ResponseStatus(HttpStatus.OK)
    public List<Product> getAllProducts() {return productService.getAllProducts();}

    @GetMapping("/test")
    @ResponseStatus(HttpStatus.OK)
    public void addTest(){
        productService.addTestProducts();
    }
//Crud

    @PutMapping("/update")
    @ResponseStatus(HttpStatus.OK)
    public void updateProduct(@RequestBody Product product) {
        productService.updateProduct(product.getUuid(),product);
    }
    @GetMapping("/{id}")
    public Product getUser(@PathVariable UUID id) {return productService.getProductByUuid(id); }

    @DeleteMapping("/remove/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteProduct(@PathVariable UUID id) {productService.deleteProductById(id);}

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public ProductResponse createProduct(@RequestBody ProductRequest productRequest){
        ProductResponse productResponse = productService.createProduct(productRequest);
        return productResponse;
//        ProductResponse productResponce = new ProductResponse();

    }





}
