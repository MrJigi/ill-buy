package com.productservice.dto.Product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponse {

    private UUID id;
    private Integer quantity;
    private String SkuCode;
    private String productName;
    private String description;
    private String category;
    private Float price;
    private Float discountPrice;
    private Date creationDate;
    private Boolean sellableOnline;
    private String otherColors;
    private String productPicture;
}
