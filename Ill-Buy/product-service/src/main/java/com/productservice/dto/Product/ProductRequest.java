package com.productservice.dto.Product;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequest {

    @Id
    @GeneratedValue()
    private UUID productId;
//    private Integer id;
    private Integer quantity;
    private String SkuCode;
    private String productName;
    private String description;
    private String category;
    private Float price;
    private Float discountPrice;
    private Date creationDate;
    private Boolean sellableOnline;
    private String otherColors;
    private String productPicture;

}
