package com.productservice.service;

import com.productservice.dto.Product.ProductRequest;
import com.productservice.dto.Product.ProductResponse;
import com.productservice.model.Product;
import com.productservice.repository.IProductRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Service;

import javax.print.attribute.standard.DateTimeAtCreation;
import java.text.Format;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
//@RequiredArgsConstructor
@Slf4j
public class ProductService {

    private final IProductRepo IProductRepository;
    @Autowired
    public ProductService(IProductRepo iProductRepo) { this.IProductRepository = iProductRepo;}
    public List<Product> getAllProducts() {
        return IProductRepository.findAll();
    }

    public Product updateProduct(UUID id, Product product) {
        Product existingUser = IProductRepository.findById(id).orElse(null);
        if (existingUser != null) {
            IProductRepository.save(product);
        }
        log.info("Product "+ existingUser + " has been updated");
        return existingUser;
    }
    public ProductResponse productResponseBuilder(ProductRequest productRequest){
        ProductResponse productResponse = new ProductResponse();
            productResponse.setId(productRequest.getProductId());
            productResponse.setQuantity(productRequest.getQuantity());
            productResponse.setSkuCode(UUID.randomUUID().toString());
            productResponse.setProductName(productRequest.getProductName());
            productResponse.setDescription(productRequest.getDescription());
            productResponse.setCategory(productRequest.getCategory());
            productResponse.setPrice(productRequest.getPrice());
            productResponse.setDiscountPrice(productRequest.getDiscountPrice());
            productResponse.setCreationDate(productRequest.getCreationDate());
            productResponse.setSellableOnline(productRequest.getSellableOnline());
            productResponse.setOtherColors(productRequest.getOtherColors());
            productResponse.setProductPicture(productRequest.getProductPicture());

            return productResponse;

    }
    public Product addTestProducts() {
        Product product = new Product(
                UUID.randomUUID(),
                5,
                "SKU",
                "ipone",
                "It goes brrr",
                "Electronics",
                159.65f,
                70.01f,
                Date.from(Instant.now()),
                true,
                "Yes",
                ""
                );
        return IProductRepository.save(product);
    }
    public ProductResponse createProduct(ProductRequest productRequest){

        ProductResponse productResponse = productResponseBuilder(productRequest);

        if(productResponse!=null){
            Product newProduct = Product.builder()
//                .id(1)
                    .quantity(productRequest.getQuantity())
                    .skuCode(UUID.randomUUID().toString())
                    .productName(productRequest.getProductName())
                    .description(productRequest.getDescription())
                    .category(productRequest.getCategory())
                    .price(productRequest.getPrice())
                    .discountPrice(productRequest.getDiscountPrice())
                    .creationDate(productRequest.getCreationDate())
                    .sellableOnline(productRequest.getSellableOnline())
                    .otherColors(productRequest.getOtherColors())
                    .productPicture(productRequest.getProductPicture())
                    .build();

            IProductRepository.save(newProduct);
            log.info("Product"+ productRequest.getProductName() + "has been saved");
            return productResponse;
        }
        else {
            throw new IllegalArgumentException("Incorrect request");
        }

    }
    public void deleteProductById(UUID uuid) {
        IProductRepository.deleteProductByUuid(uuid);
    }
    public Product getProductByUuid(UUID id) {
        return IProductRepository.findById(id).orElse(null);
    }

    public Product getProductBySkuCode(String skuCode){
        return IProductRepository.getProductBySkuCode(skuCode);
    }


}
