package com.orderservice.dto.order;

import com.orderservice.model.OrderItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderRequest {
    private UUID customerNumber;
    private UUID orderNumber;
    private List<OrderItemDto> orderItemDtoList;
    private String orderDescription;
}
