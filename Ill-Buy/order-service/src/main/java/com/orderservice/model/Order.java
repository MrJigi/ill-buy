package com.orderservice.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import java.util.List;
import java.util.UUID;

@Entity
@Data
@Table(name = "orders")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Order {

    //Possibly add user id to link the user who ordered it
    @Id
//    @GeneratedValue(generator="increment")
    private UUID id;
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID orderNumber;
    private UUID customerNumber;
    private String orderDescription;
    @OneToMany(cascade = CascadeType.ALL)
    private List<OrderItem> orderItemList;

}
