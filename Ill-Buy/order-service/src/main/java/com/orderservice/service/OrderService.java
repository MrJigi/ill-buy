package com.orderservice.service;

import com.orderservice.dto.order.*;
import com.orderservice.event.OrderPlacedEvent;
import com.orderservice.model.Order;
import com.orderservice.model.OrderItem;
import com.orderservice.repository.IOrderRepo;
import io.micrometer.observation.ObservationRegistry;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
@Transactional
//@AllArgsConstructor
@RequiredArgsConstructor
public class OrderService {

    private final IOrderRepo iOrderRepo;
    private final WebClient.Builder webClientBuilder;
//    private final Tracer tracer;
    private final KafkaTemplate<String,OrderPlacedEvent> kafkaTemplate;
    private final ObservationRegistry observationRegistry;
    private final ApplicationEventPublisher applicationEventPublisher;

    public List<Order> getAllOrders() {
        return iOrderRepo.findAll();
    }

    public OrderResponse orderResponseBuilder(OrderRequest orderRequest){
        if (orderRequest!= null){
            OrderResponse order = new OrderResponse();
            order.setOrderDescription(orderRequest.getOrderDescription());
            order.setOrderNumber(orderRequest.getOrderNumber());
            return order;
        }
        return null;
    }

    public List<Order> getUserOrders(UUID userId){
        List<Order> orderList = iOrderRepo.getOrdersById(userId);

               // iOrderRepo.getOrdersById(userId);



//        UserResponse[] userResponses = webClientBuilder.build().get()
//                .uri("http://user-service/api/user",
//                        uriBuilder -> uriBuilder.queryParam("id",skuCodes).build())
//                .retrieve()
//                .bodyToMono(InventoryResponse[].class)
//                .block();

        return orderList;
    }
    public void placeOrder(OrderRequest orderRequest){
        Order order = new Order();
//        order.setOrderNumber(UUID.randomUUID());

        List<OrderItem> orderItemList = orderRequest.getOrderItemDtoList()
                .stream()
                .map(this::mapToDto)
                .toList();

        order.setOrderItemList(orderItemList);

        List<String> skuCodes = order.getOrderItemList()
                .stream()
                .map(OrderItem::getSkuCode)
                .toList();

//        KeycloakAuthenticationToken authenticationToken
//        UUID userId =

        //Calling inventory service to check up if product is in stock
        InventoryResponse[] inventoryResponses = webClientBuilder.build().get()
                .uri("http://inventory-service/api/inventory",
                        uriBuilder -> uriBuilder.queryParam("skuCode",skuCodes).build())
                .retrieve()
                .bodyToMono(InventoryResponse[].class)
                .block();

//        UserResponse userResponse = webClientBuilder.build().get()
//                .uri("http//user-service/api/user",
//                        uriBuilder -> uriBuilder.queryParam("UUID").build())
//                .retrieve()
//                .bodyToMono(UserResponse.class)
//                .block();

        //Checks result if any matches found
        boolean productInStock = Arrays.stream(inventoryResponses)
                .allMatch(InventoryResponse::isInStock);

        if(productInStock){
            iOrderRepo.save(order);
            kafkaTemplate.send("notificationTopic", new OrderPlacedEvent(order.getOrderNumber().toString()));
        }
        else{
            throw new IllegalArgumentException("Product not in stock");
        }



    }
    public Order addTestOrder() {
        Order order = new Order(
//                146,
//                5,
//                "SKU",
//                "ipone",
//                "It goes brrr",
//                "Electronics",
//                159.65f,
//                70.01f,
//                Date.from(Instant.now()),
//                true,
//                "Yes",
//                ""
        );
        return iOrderRepo.save(order);
    }

    public Order createOrder(OrderRequest orderRequest){

        if(orderRequest != null){
            Order newOrder = Order.builder()
                    .orderNumber(UUID.randomUUID())
                    .orderDescription(orderRequest.getOrderDescription())
//                .quantity(productRequest.getQuantity())
//                .skuCode(productRequest.getSkuCode())
//                .productName(productRequest.getProductName())
//                .description(productRequest.getDescription())
//                .category(productRequest.getCategory())
//                .price(productRequest.getPrice())
//                .discountPrice(productRequest.getDiscountPrice())
//                .creationDate(productRequest.getCreationDate())
//                .sellableOnline(productRequest.getSellableOnline())
//                .otherColors(productRequest.getOtherColors())
//                .productPicture(productRequest.getProductPicture())
                    .build();

        iOrderRepo.save(newOrder);
        log.info("Order number "+ newOrder.getOrderNumber() + " has been saved");
        return newOrder;
        }
        else{
            throw new RuntimeException("The order has formatting issues");
        }
    }
    public Order getOrderByUUID(UUID id) {
        return iOrderRepo.findById(id).orElse(null);
    }

//    public void deleteOrder(UUID id){IOrderRepo.deleteAllById(id);}
    private OrderItem mapToDto(OrderItemDto orderItemDto){
        OrderItem orderItems = new OrderItem();
        orderItems.setQuantity(orderItemDto.getQuantity());
        orderItems.setSkuCode(orderItemDto.getSkuCode());
        orderItems.setPrice(orderItemDto.getPrice());
        return orderItems;
    }

    public void removeAllUserOrders(UUID uuid) {
        iOrderRepo.removeAllByCustomerNumber(uuid);
    }
}
