package com.securityservice.config;

import com.google.common.net.HttpHeaders;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
//@ConfigurationProperties(prefix ="jwt")
@NoArgsConstructor
public class JwtConfig {

    @Value("${IllBuy.jwt.secretKey}")
    private String secretKey;
    @Value("${IllBuy.jwt.tokenPrefix}")
    private String tokenPrefix;
    @Value("${IllBuy.jwt.tokenExpirationAfterDays}")
    private Integer tokenExpirationAfterDays;

    public JwtConfig(String secretKey, String tokenPrefix, Integer tokenExpirationAfterDays) {
        this.secretKey = secretKey;
        this.tokenPrefix = tokenPrefix;
        this.tokenExpirationAfterDays = tokenExpirationAfterDays;
    }
    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getTokenPrefix() {
        return tokenPrefix;
    }

    public void setTokenPrefix(String tokenPrefix) {
        this.tokenPrefix = tokenPrefix;
    }

    public Integer getTokenExpirationAfterDays() {
        return tokenExpirationAfterDays;
    }

    public void setTokenExpirationAfterDays(Integer tokenExpirationAfterDays) {
        this.tokenExpirationAfterDays = tokenExpirationAfterDays;
    }
    public String getAuthorizationHeader() { return HttpHeaders.AUTHORIZATION;}


}
