package com.userservice.controller;

import com.netflix.discovery.converters.Auto;
import com.userservice.dto.User.UserCredentials;
import com.userservice.model.Users;
import com.userservice.repository.UserRepository.IUserRepo;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/api/auth")
public class AuthorizationController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IUserRepo userRepository;

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody UserCredentials userCredentials) {
        // Retrieve user from the database based on username
        Users user = userRepository.findByUsername(userCredentials.getUsername());

        // Check if user exists and password is correct
        if (user == null || !passwordEncoder.matches(userCredentials.getPassword(), user.getPassword())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        // Generate JWT token
        String token = generateJwtToken(user.getUuid(), user.getUsername());

        // Return the token as the response

        return ResponseEntity.ok(token);
    }

    private String generateJwtToken(UUID uuid, String username) {
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("uuid", uuid);
        claims.put("username",username);
        // Add any additional claims as needed

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, "yourSecretKey")
                .compact();
    }
}
