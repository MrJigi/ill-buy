package com.userservice.service.UserService;

import com.userservice.dto.User.UserRequest;
import com.userservice.dto.User.UserResponse;
import com.userservice.model.Users;
import com.userservice.repository.UserRepository.IUserRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.UUID;

@Service
//@RequiredArgsConstructor
@Slf4j
public class UserService {


    private final IUserRepo IUserRepository;


    @Autowired
    public UserService(IUserRepo IUserRepository) {
        this.IUserRepository = IUserRepository;
    }

//    public void createUser(UserRequest user){
//        Users newUser = Users.builder()
////                .uuid(UUID.randomUUID())
//                .id(user.getId())
//                .username(user.getUsername())
//                .password(user.getPassword())
//                .email(user.getEmail())
//                .firstName(user.getFirstName())
//                .lastName(user.getLastName())
//                .role("USER")
//                .build();
    public UserResponse userResponseBuilder(UserRequest userRequest){
        UserResponse userResponse = new UserResponse();
        userResponse.setId(userResponse.getId());
        userResponse.setEmail(userRequest.getEmail());
        userResponse.setUsername(userRequest.getUsername());
        userResponse.setPassword(userRequest.getPassword());
        userResponse.setFirstName(userRequest.getFirstName());
        userResponse.setLastName(userRequest.getLastName());
//        userResponse.setRoles(userRequest.getRoles());


        return userResponse;

    }
    public List<Users> getAllUsers() {
        return IUserRepository.findAll();
    }

    @EventListener(ApplicationReadyEvent.class)
    public Users addTestUsers() {
        Users users = new Users(
                UUID.randomUUID(),
                (long)1,
                "Usagi",
                "Minako",
                "usagi@gmail.com",
                "Usagi",
                "password",
                "Admin");
        return IUserRepository.save(users);
    }

    public Users getUserById(UUID id) {
        return IUserRepository.findById(id).orElse(null);
    }

    public Users addUser(Users user) {
        return IUserRepository.save(user);
    }

    public UserResponse createUser(UserRequest user){
        UserResponse response = userResponseBuilder(user);

        Users newUser = Users.builder()
//                .uuid(UUID.randomUUID())
//                .id(user.getId())
                .username(user.getUsername())
                .password(user.getPassword())
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .role("USER")
                .build();

        IUserRepository.save(newUser);
        log.info("User "+ user.getUsername() + " has been saved");

        return response;
    }
    public UserResponse createAdmin(UserRequest user){
        UserResponse response = userResponseBuilder(user);

        Users newUser = Users.builder()
                .uuid(UUID.randomUUID())
                .username(user.getUsername())
                .password(user.getPassword())
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .role("ADMIN")
                .build();

        IUserRepository.save(newUser);
        log.info("Admin "+ user.getUsername() + "has been saved");

        return response;
    }
    public void deleteUserById(UUID id) {
        IUserRepository.deleteById(id);
        log.info("User "+ id + " has been deleted");
    }

    public Users updateUser(UUID id, Users user) {
        Users existingUser = IUserRepository.findById(id).orElse(null);
        if (existingUser != null) {
            IUserRepository.save(user);
        }
        log.info("User "+ existingUser + " has been updated");
        return existingUser;
    }
}
