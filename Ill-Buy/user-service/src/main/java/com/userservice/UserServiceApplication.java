package com.userservice;

import com.userservice.service.UserService.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.event.EventListener;

@EnableDiscoveryClient
@SpringBootApplication
@Slf4j
public class UserServiceApplication {

//    @Value("${spring.datasource.url}")
//    private String DbName;
    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class, args);
    }

//    @EventListener(ApplicationReadyEvent.class)
//    public void addUserToRepo(){
//
//    }
//    @EventListener(ApplicationReadyEvent.class)
//    public void doSomethingAfterStartup() {
//        log.info(DbName);
//    }

}
