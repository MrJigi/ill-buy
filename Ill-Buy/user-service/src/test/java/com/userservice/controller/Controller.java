package com.userservice.controller;

import com.userservice.model.Users;
import com.userservice.service.UserService.UserService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.reactivestreams.Publisher;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.*;
import static org.springframework.http.RequestEntity.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static reactor.core.publisher.Mono.when;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
public class Controller {

    private MockMvc mockMvc;

    @Mock
    private UserService userService;


//    @Test
//    public void testGetUserById() throws Exception {
//        when(userService.getUserById())
//                .thenReturn(Collections.singletonList());
//    }

//    @Test
//    public void testGetUserByUser() throws Exception {
//
//        assert()
//    verify()
//    }

/*    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(new UserController(userService)).build();
    }


    @Test
    void getAllUsers() throws Exception {
        List<Users> usersList = new ArrayList<>();
        usersList.add(new Users( UUID.randomUUID(),
                1,
                "Usagi",
                "Minako",
                "usagi@gmail.com",
                "Usagi",
                "password",
                "Admin"));
        usersList.add(new Users( UUID.randomUUID(),
                2,
                "Usagis",
                "Minakos",
                "usagis@gmail.com",
                "Usagis",
                "passwords",
                "Admin"));

        when((Publisher<?>) userService.getAllUsers()).thenReturn(usersList);

        mockMvc.perform(get("/api/user/getAll"))
                .andExpect(status().isOk())
                .andExpect(content().json("[{uuid: '...', name: 'John'}, {uuid: '...', name: 'Jane'}]"));

        verify(userService, times(1)).getAllUsers();
        verifyNoMoreInteractions(userService);
    }*/


//    @Test
//    void testAddUser() throws Exception {
//        Users user = new Users( UUID.randomUUID(),
//                1,
//                "Usagi",
//                "Minako",
//                "usagi@gmail.com",
//                "Usagi",
//                "password",
//                "Admin");
//
//        when((Publisher<?>) userService.addUser(any(Users.class))).thenReturn(user);
//
//        mockMvc.perform((RequestBuilder) post("/api/user")
//                        .contentType(MediaType.APPLICATION_JSON)
//                .body("{\"uuid\": \"...\", \"name\": \"John\"}"))
//                .andExpect(status().isCreated())
//                .andExpect(content().json("{uuid: '...', name: 'John'}"));
//
//        verify(userService, times(1)).addUser(any(Users.class));
//        verifyNoMoreInteractions(userService);
//    }


}
