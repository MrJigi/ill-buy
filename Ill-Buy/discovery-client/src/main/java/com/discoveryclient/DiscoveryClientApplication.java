package com.discoveryclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
@EnableDiscoveryClient
public class DiscoveryClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(DiscoveryClientApplication.class, args);
    }
}

@RestController
class DiscoveryController {

    private DiscoveryClient discoveryClient;

    DiscoveryController (DiscoveryClient discoveryClient){this.discoveryClient = discoveryClient;}


    @RequestMapping("/service-instance/{applicationName}")
    public List<ServiceInstance> serviceInstanceList(@PathVariable String applicationName){
        return this.discoveryClient.getInstances(applicationName);

    }


}
